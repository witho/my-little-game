extends KinematicBody

export var walk_speed = 5 * 0.28
export var run_speed = 10 * 0.28
export var acceleration = 10
export var gravity = 9.8
export var jump_height = 2
export var mouse_sensitivity = 0.01

onready var head = $Head
onready var camera = $Head/Camera
var jump_power
onready var breadcrumb = load("res://Breadcrumb.tscn")
onready var hand = $Head/Hand

var velocity = Vector3()
var up = Vector3.UP

func _ready():
	var t = sqrt(jump_height * 2 / gravity)
	jump_power = t * gravity

func _input(event):
	if event is InputEventMouseMotion:
		head.rotate_y(-event.relative.x * mouse_sensitivity)
		camera.rotation.x = clamp(camera.rotation.x - event.relative.y * mouse_sensitivity, -PI/2, PI/2)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	var direction = Vector3.ZERO
	
	if Input.is_action_pressed("move_forward"):
		direction += Vector3.FORWARD
	elif Input.is_action_pressed("move_backward"):
		direction += Vector3.BACK
		
	if Input.is_action_pressed("move_left"):
		direction += Vector3.LEFT
	elif Input.is_action_pressed("move_right"):
		direction += Vector3.RIGHT
	
	direction = direction.rotated(Vector3.UP, head.rotation.y).normalized()
	var y = velocity.y
	velocity = velocity.linear_interpolate(direction * (run_speed if Input.is_action_pressed("run") else walk_speed), acceleration * delta)
	velocity.y = y
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_power
	velocity.y -= gravity * delta
	
	if Input.is_action_just_pressed("left_click"):
		var breadcrumbInstance = breadcrumb.instance()
		breadcrumbInstance.global_transform = hand.global_transform
		get_tree().get_root().add_child(breadcrumbInstance)

	var v_eff = move_and_slide(velocity, Vector3.UP, true)
	velocity.y = v_eff.y
	for i in range(0, get_slide_count()):
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("World"):
			if direction.length_squared() < 0.1:
				up = collision.normal
