extends Node

var chunk_size = 16
var chunk_subdiv = 32

func chunk(pos):
	return Vector3(pos.x / chunk_size, pos.y / chunk_size, pos.z / chunk_size)

func position(chunk):
	return Vector3(chunk.x * chunk_size, chunk.y * chunk_size, chunk.z * chunk_size)
