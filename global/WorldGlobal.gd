extends Node

var seed_text = ''
var seed_hash = 0
var noise

func _ready():
	noise = OpenSimplexNoise.new()
	noise.octaves = 4
	noise.period = 64
	noise.persistence = 0.8
