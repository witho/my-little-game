extends Spatial

func generate():
	var st = SurfaceTool.new()

	st.begin(Mesh.PRIMITIVE_TRIANGLES)

	st.set_material($MeshInstance.get_surface_material(0))
	var delta = float(GameGlobal.chunk_size) / GameGlobal.chunk_subdiv
	var x = 0.0
	while x <= GameGlobal.chunk_size:
		var z = 0.0
		while z <= GameGlobal.chunk_size:
			make_quad(st, x, z, delta, WorldGlobal.noise, 15)
			z += delta
		x += delta

	st.generate_normals()
	st.index()
	var mesh = st.commit()
	$MeshInstance.set_mesh(mesh)
	$CollisionShape.set_shape(mesh.create_trimesh_shape())
	
func make_quad(st, x, z, delta, noise, mult):
	var nx = translation.x + x
	var nz = translation.z + z
	st.add_vertex(Vector3(x, noise.get_noise_2d(nx, nz) * mult, z))
	st.add_vertex(Vector3(x + delta, noise.get_noise_2d(nx + delta, nz) * mult, z))
	st.add_vertex(Vector3(x + delta, noise.get_noise_2d(nx + delta, nz + delta) * mult, z + delta))
	st.add_vertex(Vector3(x, noise.get_noise_2d(nx, nz) * mult, z))
	st.add_vertex(Vector3(x + delta, noise.get_noise_2d(nx + delta, nz + delta) * mult, z + delta))
	st.add_vertex(Vector3(x, noise.get_noise_2d(nx, nz + delta) * mult, z + delta))
	
