extends Spatial

export(NodePath) var player_path
var player
var chunk = preload("Chunk.tscn")

func _ready():
	if player_path:
		player = get_node(player_path)
		var player_chunk = GameGlobal.chunk(player.translation)
		for x in range(-1, 2):
			for z in range(-1, 2):
				var c = chunk.instance()
				c.translation = GameGlobal.position(player_chunk + Vector3(x, 0, z))
				c.generate()
				add_child(c)
