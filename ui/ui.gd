extends CanvasLayer

onready var fps = $MarginContainer/VBoxContainer/HBoxContainer/Fps

func _on_s1_timeout():
	var txt = '%f/%.3fms/%.3fms' % [ Performance.get_monitor(Performance.TIME_FPS), Performance.get_monitor(Performance.TIME_PROCESS) * 1000, Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS) * 1000]
	fps.set_text(txt)


func _process(delta):
	if Input.is_action_just_pressed("toggle_vsync"):
		OS.set_use_vsync(!OS.vsync_enabled)
