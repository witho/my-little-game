extends KinematicBody

export var ACCELERATION = 12
export var MAX_SPEED = 1
export var FRICTION = 200
export var WANDER_TARGET_RANGE = 4

enum {
	IDLE,
	WANDER
}

var velocity = Vector3.ZERO

var state = IDLE

onready var Body = $Body
onready var Hitbox = $Hitbox
onready var wanderController = $WanderController
onready var animationPlayer = $AnimationPlayer

func _ready():
	state = pick_random_state([IDLE, WANDER])

func _process(delta):
	match state:
		IDLE:
			velocity = velocity.move_toward(Vector3.ZERO, FRICTION * delta)
			# On timeout, pick a random state
			if wanderController.get_time_left() == 0:
				update_wander()
		
		WANDER:
			# On timeout, pick a random state
			if wanderController.get_time_left() == 0:
				update_wander()
			# Move toward target_position
			accelerate_towards_point(wanderController.target_position, delta)
			look_at(wanderController.target_position, Vector3.UP)
			# Check if wander is done
			if transform.origin.distance_to(wanderController.target_position) <= WANDER_TARGET_RANGE:
				update_wander()
	
	velocity = move_and_slide(velocity)

func accelerate_towards_point(point, delta):
	velocity = velocity.move_toward(point * MAX_SPEED, ACCELERATION * delta)

func pick_random_state(state_list):
	state_list.shuffle()
	return state_list.pop_front()

func update_wander():
	state = pick_random_state([IDLE, WANDER])
	wanderController.start_wander_timer(rand_range(1, 3))
