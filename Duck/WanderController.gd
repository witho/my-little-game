extends Spatial

export(int) var wander_range = 8

var start_position = Vector3.ZERO
var target_position = Vector3.ZERO

onready var timer = $Timer

func _ready():
	update_target_position()

func update_target_position():
	start_position = global_transform.origin
	var target_vector = Vector3(rand_range(-wander_range, wander_range), 0.0, rand_range(-wander_range, wander_range))
	target_position = start_position + target_vector

func get_time_left():
	return timer.time_left

func start_wander_timer(duration):
	timer.start(duration)

func _on_Timer_timeout():
	update_target_position()
